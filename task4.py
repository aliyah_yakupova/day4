# coding: utf-8
'''
Дана строка со словами и числами, разделенными пробелами (один пробел между словами и/или числами). Слова состоят только из букв. Вам нужно проверить есть ли в исходной строке три слова подряд. Для примера, в строке "start 5 one two three 7 end" есть три слова подряд.
Входные данные: Строка со словами (str).
Выходные данные: Ответ как логическое выражение (bool), True или False.
Примеры:
check("Hello World hello") == True
check("He is 123 man") == False
check("1 2 3 4") == False
check("bla bla bla bla") == True
check("Hi") == False
'''

def check(txt):
    txt = txt.split();
    for i in range(0, len(txt)-2):
        if txt[i].isalpha() and txt[i+1].isalpha() and txt[i+2].isalpha():
            return True;
    return False;

print check("Hello World hello");
print check("He is 123 man");
print check("1 2 3 4");
print check("bla bla bla bla");
print check("Hi");
print check("Hi Hi");