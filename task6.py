# coding: utf-8
'''
Подсчитать количество простых чисел от 0 до 300
'''

def count_dig(n):
    cnt = 0;
    k = 0;
    for i in range(2, n + 1):
        for j in range(2, i):
            if i % j == 0:
                k += 1;
        if k == 0:
            cnt += 1;
        else:
            k = 0;
    return cnt;

print count_dig(300);