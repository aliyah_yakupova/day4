# coding: utf-8
import math as m;
import calendar as c;

#вернет число 2
def get_2():
    return 2;
print get_2();

#примет число; вернет число в кубе
def get_cube(n):
    return n**3;
print str(get_cube(2));

#примет число; вернёт его факториал
#v1
def get_factorial(n):
    return m.factorial(n);
#v2
def get_factorial(n):
    if n == 0:
        return 1;
    else:
        return n * get_factorial(n - 1);
print get_factorial(5);

#примет два числа; вернет меньшее
#v1
def get_min(n1, n2):
    return min(n1, n2);
#v2
def get_min(n1, n2):
    if n1 < n2:
        return n1;
    else:
        return n2;
print get_min(1, 2);

#примет два числа; вернет, то число, у которого меньше цифр
def get_min_len(n1, n2):
    if len(str(n1)) < len(str(n2)):
        return n1;
    else:
        return n2;
print get_min_len(10, 100);

#примет два числа; вернёт гипотенузу треугольника с заданными катетами
#v1
def get_hypot(n1, n2):
    return m.hypot(n1, n2);
#v2
def get_hypot(n1, n2):
    return m.sqrt(n1**2 + n2**2);
print get_hypot(3, 4);

#примет натуральное число; вернёт его последнюю цифру
def get_last_digit(n):
    return int(str(n)[-1]);
print get_last_digit(12345);

#примет трёхзначное число, вернёт сумму его цифр
#примет число с произвольным количеством цифр, вернёт сумму его цифр
def get_sum_digit(n):
    return sum([int(d) for d in str(n)]);
print get_sum_digit(12345);

#примет список чисел; вернет количество чисел, равных двум
def get_equal_2(nlst):
    return nlst.count(2);
print get_equal_2([2,5,5.4,32,22,2.1,2,2.0]);

#примет список чисел, число для сравнения; вернет количество вхождений числа для сравнения в список
def get_equal_num(nlst, n):
    return nlst.count(n);
print get_equal_num([2,5,5.4,32,22,2.1,2,2.0],22);

#предыдущий пример; со значением по умолчанию для второго параметра равным 2
def get_equal_dnum(nlst, n = 2):
    return nlst.count(n);
print get_equal_dnum([2,5,5.4,32,22,2.1,2,2.0]);
print get_equal_dnum([2,5,5.4,32,22,2.1,2,2.0],22);

#примет список с разными типами элементов, элемент для сравнения (значение по умолчанию можно выбрать произвольное); вернет количество вхождений элемента для сравнения в список
def get_equal_dnum_any(lst, n = 't'):
    return lst.count(n);
print get_equal_dnum_any(['t',2,5,5.4,32,22,2.1,2,2.0]);
print get_equal_dnum_any(['t',2,5,5.4,32,22,2.1,2,2.0],22);

#принимает неизвестное количество элементов строкового типа; вернет самую длинную строку
def get_max_len(*el):
    d = {len(str(a)): a for a in el}
    return d[max(d.keys())];
print get_max_len(2, 555, 'ololo', 3.5495, 8.5495);

#принимает неизвестное количество элементов строкового типа; вернет самую длинную строку и ее длину (ответ будет типа кортежа)
def get_max_len(*el):
    d = {len(str(a)): a for a in el}
    return d[max(d.keys())], max(d.keys());
print get_max_len(2, 555, 'ololo', 3.5495, 8.5495);

#примет год; вернёт ответ на вопрос, является ли он високосным
def check_leap_year(y):
    return c.isleap(y); #y%4 == 0 and y%100 != 0 or y%400 == 0:
print check_leap_year(2016);

#примет список; произведёт циклический сдвиг элементов списка вправо (A[0] переходит на место A[1], A[1] на место A[2], ..., последний элемент переходит на место A[0]).
def get_right_shift(lst):
    return lst[-1:] + lst[:-1];
print get_right_shift([2, 3, 4, 5, 1]);
