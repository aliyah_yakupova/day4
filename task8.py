# coding: utf-8
'''
список комментариев
'''

comments = []
comment = {}
comments.append(comment)
comment['id'] = 1
comment['parent_id'] = 0
comment['autor'] = 'Гарри'
comment['time'] = '15:00'
comment['text'] = 'Как перевернуть человека вверх ногами?'
comment = {}
comments.append(comment)
comment['id'] = 2
comment['parent_id'] = 1
comment['autor']= 'Гермиона'
comment['time']= '15:05'
comment['text'] = 'Левикорпус'
comment = {}
comments.append(comment)
comment['id'] = 3
comment['parent_id'] = 2
comment['autor'] = 'Гарри'
comment['time'] = '15:15'
comment['text'] = 'Спасибо сработало'
comment = {}
comments.append(comment)
comment['id'] = 4
comment['parent_id'] = 0
comment['autor'] = 'Рон'
comment['time'] = '15:16'
comment['text'] = 'Как спастись от левикорпуса?'
comment = {}
comments.append(comment)
comment['id'] = 5
comment['parent_id'] = 4
comment['autor'] = 'Полумна'
comment['time'] = '15:17'
comment['text'] = 'Либеракорпус'
comment = {}
comments.append(comment)
comment['id'] = 6
comment['parent_id'] = 3
comment['autor'] = 'Гермиона'
comment['time'] = '15:22'
comment['text'] = 'На ком пробуешь?'
comment = {}
comments.append(comment)
comment['id'] = 7
comment['parent_id'] = 4
comment['autor'] = 'Джинни'
comment['time'] = '15:25'
comment['text'] = 'Кто тебя обижает?'
comment = {}
comments.append(comment)
comment['id'] = 8
comment['parent_id'] = 7
comment['autor'] = 'Рон'
comment['time'] = '15:26'
comment['text'] = 'Гарри, кто же еще?'
comment = {}
comments.append(comment)
comment['id'] = 9
comment['parent_id'] = 6
comment['autor'] = 'Гермиона'
comment['time'] = '15:28'
comment['text'] = 'А все поняла))'

def print_chat(cm, dash):
    print(dash + '[{}] {}:'.format(cm['time'], cm['autor']))
    print(dash + '{}'.format(cm['text']))

def get_chat(pid, dash): #todo оптимизировать!
    for cm in comments:
        if cm['parent_id'] == pid:
            print_chat(cm, dash);
            get_chat(cm['id'], dash + '--');

get_chat(0,'--');