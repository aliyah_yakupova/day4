# coding: utf-8
'''
Дан кусок текста. Соберите все заглавные буквы в одно слово в том порядке как они встречаются в куске текста.
Например: текст = "How are you? Eh, ok. Low or Lower? Ohhh.", если мы соберем все заглавные буквы, то получим сообщение "HELLO".
Входные данные: Текст как строка (юникод).
Выходные данные: Секретное сообщение как строка или пустая строка.
Пример:
find_message("How are you? Eh, ok. Low or Lower? Ohhh.") == "HELLO"
find_message("hello world!") == ""
'''

def find_message(txt):
    return ''.join([x for x in txt if x.isupper()]);

print find_message("How are you? Eh, ok. Low or Lower? Ohhh.");
print find_message("hello world!");